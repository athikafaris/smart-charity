<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Summery
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
  <!-- CSS Files -->
  <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../assets/demo/demo.css" rel="stylesheet" />
  <link rel="stylesheet" href="/css/treeview.css">
  <script src="datepicker.js"></script>
  <style media="screen">

    .treeview-menu{
      font-size: 15px;
    }
    .payin{
      width: 30%;
    }
    .dropdown {
  position: absolute;
  line-height: 20px;
  font-size: 12px;
  width: 140px;
  display: none;
  background: #f6f6f6;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
a.previous, a.next, .days a {
  cursor: pointer;
  text-align: center;
  display: inline-block;
  width: 20px;
}
.selected {
  font-weight: bold;
  background: #8a46ff;
  color: #ffffff;
}
input.datepicker:focus + div.dropdown { display: block; }
input.datepicker:focus + div.dropdown, div.dropdown:hover { display: block; }
.btn2{
  width: 80%
}
.btn3
{
  padding: 5px;
  background-color: silver;
  border: 1 solid black;
  cursor: pointer;
}
  </style>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="{{ url('/dashboard')}}" class="simple-text logo-normal">
          {{ Auth::user()->fname }}
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item  ">
            <a class="nav-link" href="{{ url('/dashboard')}}">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="./user.html">
              <i class="material-icons">person</i>
              <p>User Profile</p>
            </a>
          </li>
          <li class="nav-item  ">
            <a class="nav-link" href="pending">
              <i class="material-icons">content_paste</i>
              <p>pending List</p>
            </a>
          </li>
          <li class="nav-item  treeview">
            <a class="nav-link" href="#">
              <i class="material-icons">library_books</i>
              <p>payments</p>
            </a>
            <ul class="treeview-menu ">
              <li class="btn"> <a href="{{url('/memberpays')}}">memberpayment</a> </li>
              <li class="btn"> <a href="{{url('/guestpay')}}" >guestpayment</a> </li>
            </ul>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="{{url('/registration')}}">
              <i class="material-icons">bubble_chart</i>
              <p>Registration</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="{{url('manage')}}">
              <i class="material-icons">location_ons</i>
              <p>Manage Account</p>
            </a>
            <li class="nav-item active">
              <a class="nav-link" href="summery">
                <i class="material-icons">content_paste</i>
                <p>Summery</p>
              </a>
            </li>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="./notifications.html">
              <i class="material-icons">notifications</i>
              <p>Notifications</p>
            </a>
          </li>
          <!-- <li class="nav-item active-pro ">
                <a class="nav-link" href="./upgrade.html">
                    <i class="material-icons">unarchive</i>
                    <p>Upgrade to PRO</p>
                </a>
            </li> -->
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="pending">Payment Summary</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="{{ url('/dashboard')}}">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Mike John responded to your email</a>
                  <a class="dropdown-item" href="#">You have 5 new tasks</a>
                  <a class="dropdown-item" href="#">You're now friend with Andrew</a>
                  <a class="dropdown-item" href="#">Another Notification</a>
                  <a class="dropdown-item" href="#">Another One</a>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#pablo">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      @if(Session::has('success'))
          <div class="alert alert-success" role="alert">{{ Session::get('success') }}</div>
      @endif
      <div class="content" ng-app="" ng-init="summaryin='daily'">
        <div class="container-fluid">
          <form class="form-group" action="{{url('summery')}}" method="post">
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="">From</label>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label for="">To</label>
                </div>
              </div>
            </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="text" name="from"  id="from" class="form-control" style="background-color:white;">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <input type="text" name="to"  id="to"class="form-control" style="background-color:white;">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary" name="button" style="height:35px; margin-top:0px;padding:10px;">submit</button>
                  </div>
                </div>
              </div>
            </form>


          @if(isset($data))

          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card card-header-primary" style="height:100px;">
                  <h4 class="card-title ">Total Payment Summary</h4>
                  <p class="card-category"> Payment Details of guest payments</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <th>ID</th>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Amount</th>
                        <th>Method</th>
                        <th>Number</th>
                      </thead>
                      <tbody>
                        @foreach ( $data['member'] as $memberpay )
                        <tr>
                          <td>{{ $memberpay->id }}</td>
                          <td>{{ $memberpay->date}}</td>
                          <td>{{ $memberpay->users->fname}}</td>
                          <td>{{ $memberpay->amount }}</td>
                          <td>{{ $memberpay->method}}</td>
                          <td>{{ $memberpay->paymentnumber }}</td>

                        </tr>
                        @endforeach

                        <tr>
                          <td></td>
                          <td>Total </td>
                          <td></td>
                          <td>{{$data['member']->sum('amount')}}</td>
                        </tr>
                      </tbody>
                    </table>
                    <ul id="myUL">
                      <li class="table"><span class="caret">Total &nbsp&nbsp&nbsp{{$data['member']->sum('amount')}}</span>
                        <ul class="nested">
                          <li><span class="caret">Kozhikkode &nbsp&nbsp&nbsp{{$data['member']->whereIn('user_id',$data['zone1'])->sum('amount')}}</span>
                            <ul class="nested">
                              <li><span class="caret">Kozhikode &nbsp&nbsp&nbsp{{$data['member']->whereIn('user_id',$data['unit11'])->sum('amount')}}</span>
                                <ul class="nested">
                                  @foreach($data['member']->whereIn('user_id',$data['unit11']) as $member)
                                    <table>
                                      <tr>
                                        <td>{{$member->users->fname}}</td>
                                        <td>{{$member->amount}}</td>
                                      </tr>
                                    </table>
                                  @endforeach
                                </ul>
                              </li>
                              <li><span class="caret">Kannur &nbsp&nbsp&nbsp{{$data['member']->whereIn('user_id',$data['unit12'])->sum('amount')}}</span>
                                <ul class="nested">
                                  @foreach($data['member']->whereIn('user_id',$data['unit12']) as $member)
                                    <table>
                                      <tr>
                                        <td>{{$member->users->fname}}</td>
                                        <td>{{$member->amount}}</td>
                                      </tr>
                                    </table>
                                  @endforeach
                                </ul>
                              </li>
                              <li><span class="caret">Wayanad &nbsp&nbsp&nbsp{{$data['member']->whereIn('user_id',$data['unit13'])->sum('amount')}}</span>
                                <ul class="nested">
                                  @foreach($data['member']->whereIn('user_id',$data['unit13']) as $member)
                                    <table>
                                      <tr>
                                        <td>{{$member->users->fname}}</td>
                                        <td>{{$member->amount}}</td>
                                      </tr>
                                    </table>
                                  @endforeach
                                </ul>
                              </li>
                              <li><span class="caret">Kasargod&nbsp&nbsp&nbsp{{$data['member']->whereIn('user_id',$data['unit14'])->sum('amount')}}</span>
                                <ul class="nested">
                                  @foreach($data['member']->whereIn('user_id',$data['unit14']) as $member)
                                    <table>
                                      <tr>
                                        <td>{{$member->users->fname}}</td>
                                        <td>{{$member->amount}}</td>
                                      </tr>
                                    </table>
                                  @endforeach
                                </ul>
                              </li>
                            </ul>
                          </li>
                          <li><span class="caret">Malappuram &nbsp&nbsp&nbsp{{$data['member']->whereIn('user_id',$data['zone2'])->sum('amount')}}</span>
                            <ul class="nested">
                              <li><span class="caret">Malappuram &nbsp&nbsp&nbsp{{$data['member']->whereIn('user_id',$data['unit21'])->sum('amount')}}</span>
                                <ul class="nested">
                                  @foreach($data['member']->whereIn('user_id',$data['unit21']) as $member)
                                    <table>
                                      <tr>
                                        <td>{{$member->users->fname}}</td>
                                        <td>{{$member->amount}}</td>
                                      </tr>
                                    </table>
                                  @endforeach
                                </ul>
                              </li>
                              <li><span class="caret">Palakkad &nbsp&nbsp&nbsp{{$data['member']->whereIn('user_id',$data['unit22'])->sum('amount')}}</span>
                                <ul class="nested">
                                  @foreach($data['member']->whereIn('user_id',$data['unit22']) as $member)
                                    <table>
                                      <tr>
                                        <td>{{$member->users->fname}}</td>
                                        <td>{{$member->amount}}</td>
                                      </tr>
                                    </table>
                                  @endforeach
                                </ul>
                              </li>
                              <li><span class="caret">Thrissur&nbsp&nbsp&nbsp{{$data['member']->whereIn('user_id',$data['unit23'])->sum('amount')}}</span>
                                <ul class="nested">
                                  @foreach($data['member']->whereIn('user_id',$data['unit23']) as $member)
                                    <table>
                                      <tr>
                                        <td>{{$member->users->fname}}</td>
                                        <td>{{$member->amount}}</td>
                                      </tr>
                                    </table>
                                  @endforeach
                                </ul>
                              </li>
                            </ul>
                          </li>
                          <li><span class="caret">Ernakulam &nbsp&nbsp&nbsp{{$data['member']->whereIn('user_id',$data['zone3'])->sum('amount')}}</span>
                            <ul class="nested">
                              <li><span class="caret">Kochi &nbsp&nbsp&nbsp{{$data['member']->whereIn('user_id',$data['unit31'])->sum('amount')}}</span>
                                <ul class="nested">
                                  @foreach($data['member']->whereIn('user_id',$data['unit31']) as $member)
                                    <table>
                                      <tr>
                                        <td>{{$member->users->fname}}</td>
                                        <td>{{$member->amount}}</td>
                                      </tr>
                                    </table>
                                  @endforeach
                                </ul>
                              </li>
                              <li><span class="caret">Alappuzha&nbsp&nbsp&nbsp{{$data['member']->whereIn('user_id',$data['unit32'])->sum('amount')}}</span>
                                <ul class="nested">
                                  @foreach($data['member']->whereIn('user_id',$data['unit32']) as $member)
                                    <table>
                                      <tr>
                                        <td>{{$member->users->fname}}</td>
                                        <td>{{$member->amount}}</td>
                                      </tr>
                                    </table>
                                  @endforeach
                                </ul>
                              </li>
                              <li><span class="caret">Idukki &nbsp&nbsp&nbsp{{$data['member']->whereIn('user_id',$data['unit33'])->sum('amount')}}</span>
                                <ul class="nested">
                                  @foreach($data['member']->whereIn('user_id',$data['unit33']) as $member)
                                    <table>
                                      <tr>
                                        <td>{{$member->users->fname}}</td>
                                        <td>{{$member->amount}}</td>
                                      </tr>
                                    </table>
                                  @endforeach
                                </ul>
                              </li>
                            </ul>
                          </li>
                          <li><span class="caret">Thiruvananthapuram &nbsp&nbsp&nbsp{{$data['member']->whereIn('user_id',$data['zone4'])->sum('amount')}}</span>
                            <ul class="nested">
                              <li><span class="caret">Thiruvananthapuram&nbsp&nbsp&nbsp{{$data['member']->whereIn('user_id',$data['unit41'])->sum('amount')}}</span>
                                <ul class="nested">
                                  @foreach($data['member']->whereIn('user_id',$data['unit41']) as $member)
                                    <table>
                                      <tr>
                                        <td>{{$member->users->fname}}</td>
                                        <td>{{$member->amount}}</td>
                                      </tr>
                                    </table>
                                  @endforeach
                                </ul>
                              </li>
                              <li><span class="caret">Kollam &nbsp&nbsp&nbsp{{$data['member']->whereIn('user_id',$data['unit42'])->sum('amount')}}</span>
                                <ul class="nested">
                                  @foreach($data['member']->whereIn('user_id',$data['unit42']) as $member)
                                    <table>
                                      <tr>
                                        <td>{{$member->users->fname}}</td>
                                        <td>{{$member->amount}}</td>
                                      </tr>
                                    </table>
                                  @endforeach
                                </ul>
                              </li>
                              <li><span class="caret">Kottayam &nbsp&nbsp&nbsp{{$data['member']->whereIn('user_id',$data['unit43'])->sum('amount')}}</span>
                                <ul class="nested">
                                  @foreach($data['member']->whereIn('user_id',$data['unit43']) as $member)
                                    <table>
                                      <tr>
                                        <td>{{$member->users->fname}}</td>
                                        <td>{{$member->amount}}</td>
                                      </tr>
                                    </table>
                                  @endforeach
                                </ul>
                              </li>
                              <li><span class="caret">Pathanamthitta &nbsp&nbsp&nbsp{{$data['member']->whereIn('user_id',$data['unit44'])->sum('amount')}}</span>
                                <ul class="nested">
                                  @foreach($data['member']->whereIn('user_id',$data['unit44']) as $member)
                                    <table>
                                      <tr>
                                        <td>{{$member->users->fname}}</td>
                                        <td>{{$member->amount}}</td>
                                      </tr>
                                    </table>
                                  @endforeach
                                </ul>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
      @endisset
        </div>
      </div>
      <footer class="footer">
        <div class="container-fluid">
          <nav class="float-left">
            <ul>
              <li>
                <a href="https://www.creative-tim.com">
                  Creative Tim
                </a>
              </li>
              <li>
                <a href="https://creative-tim.com/presentation">
                  About Us
                </a>
              </li>
              <li>
                <a href="http://blog.creative-tim.com">
                  Blog
                </a>
              </li>
              <li>
                <a href="https://www.creative-tim.com/license">
                  Licenses
                </a>
              </li>
            </ul>
          </nav>
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, made with <i class="material-icons">favorite</i> by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="../assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="../assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="../assets/demo/demo.js"></script>
  <script type="text/javascript" src="js/validation.js">
  </script>
  <script type="text/javascript" src="js/states.js">
  </script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <link rel="stylesheet" href="/resources/demos/style.css">
 <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</body>
<script>
  $(document).ready(function() {

    // Javascript method's body can be found in assets/js/demos.js
    // date picker
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth()+1;
    var year = d.getFullYear();
     $('#from').datepicker({ dateFormat: 'yy-mm-dd' });
     $('#to').datepicker({ dateFormat: 'yy-mm-dd' });
     $('#from').val(year+"-"+month+"-"+day);
     $('#to').val(year+"-"+month+"-"+day);
     var toggler = document.getElementsByClassName("caret");
var i;

for (i = 0; i < toggler.length; i++) {
  toggler[i].addEventListener("click", function() {
    this.parentElement.querySelector(".nested").classList.toggle("active");
    this.classList.toggle("caret-down");
  });
}
    // alert(day+"-"+month+"-"+year);
    // document.getElementById("from").innerHTML="day+"-"+month+"-"+year";
    // var monthNames = ["January", "February", "March", "April", "May", "June",
    // "July", "August", "September", "October", "November", "December"];
    // var monthday = [31,28,31,30,31,30,31,31,30,31,30,31];
    // // /datepicker
    // $('.show').hide();


    md.initDashboardPageCharts();
    $(".treeview-menu").hide();
    $(".treeview").click(function(){
      $(".treeview-menu").toggle("fast","linear");
  });
  function nextday()
  {
    alert('ajkfdjlak');
  }
  });

</script>
</html>
