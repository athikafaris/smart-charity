<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>
<body name="login">

	<div class="limiter">
		<div class="col-sm-4 col-sm-offset-4">
				@if(Session::has('error'))
						<div class="alert alert-warning" role="alert">{{ Session::get('error') }}</div>
				@endif
				@if(count($errors) > 0)
						<div class="alert alert-danger">
				@foreach($errors->all() as $error)
						<ul>{{ $error }}</ul>
				@endforeach
						</div>
				@endif
		</div>
		<div class="container-login100" style="background-image: url('images/bg-01.jpg');">
			<div class="wrap-login100 p-l-110 p-r-110 p-t-62 p-b-33">
				<form class="login100-form validate-form flex-sb flex-w" action="{{route('logincustom')}}" method="post">
					{{csrf_field()}}
					<span class="login100-form-title p-b-53">
						Sign In With
					</span>

					<a id="guest"  href="/guest"class="btn-face m-b-20">
						<i class="fa fa-user"></i>
						Guest
					</a>

					<a href="/form" class="btn-google m-b-20">

						Sign Up
					</a>
					<div class="">

					</div>
					<div class="p-t-31 p-b-9">
						<span class="txt1" id="label1">
							Username
						</span>
					</div>
					<div class="wrap-input100 validate-input" data-validate = "Username is required">
						<input class="input100" type="text" name="email" >
						<span class="focus-input100"></span>
					</div>

					<div class="p-t-13 p-b-9">
						<span class="txt1" id="label2">
							Password
						</span>

						<a id="forgot" href="/reset" class="txt2 bo1 m-l-5">
							Forgot?
						</a>
					</div>
					<div id="pass" class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="psw"  >
						<span class="focus-input100"></span>
					</div>
					<!-- <div id="mail" class="wrap-input100 validate-input" data-validate = "email is required">
						<input class="input100" type="email" name="email" >
						<span class="focus-input100"></span>
					</div> -->
					<div class="container-login100-form-btn m-t-17">
						<button class="login100-form-btn">
							Sign In
						</button>
					</div>

					<div class="w-full text-center p-t-55">
						<span class="txt2">
							Not a member?
						</span>

						<a href="/form" class="txt2 bo1">
							Sign up now
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>


	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/daterangepicker/moment.min.js"></script>
	<script src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="js/loginjs.js"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $('#guest').click(function () {
						$('#guest').html('sign in');
            $('#label1').html('name');
						$('#label2').html('email');
						$('#forgot').toggle();
						$('#mail').show();
						$('#pass').toggle();;

        });
    });

		$(document).ready(function(){
			$('#mail').hide();
});
</script> -->
</body>
</html>
