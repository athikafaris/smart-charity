<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Registration form
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="../assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="../assets/demo/demo.css" rel="stylesheet" />
  <script type="text/javascript" src="js/validation.js">
  </script>
  <script type="text/javascript" src="js/states.js">
  </script>
  <style media="screen">
    select
    {
      border: 0px;
      border-bottom: 1px solid silver;
    }
    .gender{
      margin-top: 20px;
    }
    .radio-inline
    {
      margin-left: 15px;
    }
    .error
    {
      background-color: red;
    }
  </style>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->

      <div class="sidebar-wrapper">

      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Registration Form</a>
          </div>

          <div class="collapse navbar-collapse justify-content-end">

          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="row" >
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Registraion Form</h4>
                  <p class="card-category">Complete your profile</p>
                </div>
                <div class="col-md-12">
                    @if(Session::has('error'))
                        <div class="alert alert-danger" role="alert">{{ Session::get('error') }}</div>
                    @endif
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                        <ul>{{ $error }}</ul>
                    @endforeach
                        </div>
                    @endif
                    @if(Session::has('msg'))
                        <div class="alert alert-success" role="alert">{{ Session::get('msg') }}</div>
                    @endif
                </div>
                <div class="card-body">
                  <form class="forms form-horizontal"action="{{url('store')}}" method="post">
                    {{ csrf_field() }}
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Fist Name</label>
                          <input type="text" class="form-control" name="fname" required>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Last Name</label>
                          <input type="text" class="form-control" name="lname">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">House</label>
                          <input type="text" class="form-control" name="house" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Street</label>
                          <input type="text" class="form-control" name="streets" required>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Land Mark</label>
                          <input type="text" class="form-control" name="landmark">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">pincode</label>
                          <input type="number" class="form-control" name="pin">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-2">
                        <div class="form-group">
                          <label class="bmd-label-floating">State</label>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <select class="form-group" required id="stateselect"  name="state" size="1" onchange="makeSubmenu(this.value)">
                          <option value="" >-Select State-</option>
                          <option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
                          <option value="Andhra Pradesh">Andhra Pradesh</option>
                          <option value="Arunachal Pradesh">Arunachal Pradesh</option>
                          <option value="Assam">Assam</option>
                          <option value="Bihar">Bihar</option>
                          <option value="Chandigarh">Chandigarh</option>
                          <option value="Chhattisgarh">Chhattisgarh</option>
                          <option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>
                          <option value="Daman and Diu">Daman and Diu</option>
                          <option value="Delhi">Delhi</option>
                          <option value="Goa">Goa</option>
                          <option value="Gujarat">Gujarat</option>
                          <option value="Haryana">Haryana</option>
                          <option value="Himachal Pradesh">Himachal Pradesh</option>
                          <option value="Jammu and Kashmir">Jammu and Kashmir</option>
                          <option value="Jharkhand">Jharkhand</option>
                          <option value="Karnataka">Karnataka</option>
                          <option value="Kerala">Kerala</option>
                          <option value="Lakshadweep">Lakshadweep</option>
                          <option value="Madhya Pradesh">Madhya Pradesh</option>
                          <option value="Maharashtra">Maharashtra</option>
                          <option value="Manipur">Manipur</option>
                          <option value="Meghalaya">Meghalaya</option>
                          <option value="Mizoram">Mizoram</option>
                          <option value="Nagaland">Nagaland</option>
                          <option value="Orissa">Orissa</option>
                          <option value="Pondicherry">Pondicherry</option>
                          <option value="Punjab">Punjab</option>
                          <option value="Rajasthan">Rajasthan</option>
                          <option value="Sikkim">Sikkim</option>
                          <option value="Tamil Nadu">Tamil Nadu</option>
                          <option value="Tripura">Tripura</option>
                          <option value="Uttaranchal">Uttaranchal</option>
                          <option value="Uttar Pradesh">Uttar Pradesh</option>
                          <option value="West Bengal">West Bengal</option>
                        </select>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label class="bmd-label-floating">District</label>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <select id="distselect" name="district" size="1" class="form-group">
                          <option value=""  selected>Choose District</option>
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-2">
                        <label class="gender">Gender</label>
                      </div>
                      <div class="col-md-6">
                        <label class="radio-inline">
                          <input type="radio" name="gender" class="form-group gernder-radio" value="male" required>Male
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="gender" class="form-group" value="female" required>Female
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="gender" class="form-group" value="other" required>Other
                        </label>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-2">
                        <div class="form-group">
                          <label class="bmd-label-floating">Zone</label>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <select id="countrySelect" name="zone" size="1" onchange="makeunit(this.value)" class="form-group" required>
                            <option value="" disabled selected>Choose Zone</option>
                            <option>Kozhikkode</option>
                            <option>Malappuram</option>
                            <option>Ernakulam</option>
                            <option>Thiruvananthapuram</option>
                          </select>
                      </div>
                      <div class="col-md-2">
                        <div class="form-group">
                          <label class="bmd-label-floating">Unit</label>
                        </div>
                      </div>
                      <div class="col-md-4">
                        <select id="citySelect" name="unit" size="1" class="form-group" required>
                            <option value="" >Choose Unit</option>
                            <option></option>
                          </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Phone number</label>
                          <input type="number" name="phone" class="form-control"required>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">E-mail</label>
                          <input type="email" name="email" id="eid" class="form-control" required>
                        </div>
                      </div>
                    </div>
                    <label class="bmd-label-floating" id="error" class="error" style="color:red;margin-left:52%;">Invalid Email</label>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Password</label>
                          <input type="password" class="form-control" name="passwd" required>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <input type="checkbox" name="" value="">
                          <label class="bmd-label-floating">I here by declaring that </label>
                        </div>
                      </div>
                    </div>
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">Register</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="../assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="../assets/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="../assets/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="../assets/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="../assets/demo/demo.js"></script>
</body>
<script>
$(document).ready(function() {
  $("#error").hide();
    $('#eid').blur(function() {


            var hasError = false;
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            var emailLegalReg =  /^([\w-\.]+@(?!gmail.com)(?!yahoo.com)(?!hotmail.com)(?!aol.com)([\w-]+\.)+[\w-]{2,4})?$/;

            var emailaddressVal = $("#eid").val();
            if(emailaddressVal == '') {
              $("#error").show();
                // $("#eid").after('<span class="error">Please enter your email address.</span>');
                hasError = true;
            } else if(!emailReg.test(emailaddressVal)) {
                // $("#eid").after('<span class="error">Enter a valid email address.</span>');
                $("#error").show();

                hasError = true;
            } else if(emailLegalReg.test(emailaddressVal)) {
                // $("#eid").after('<span class="error">No eamil apart from gmail, hotmail or yahoo is allowed.</span>');
                $("#error").show();
                hasError = true;
            }

            if(hasError == true) { return false; }
            else {
                // $("#UserEmail").after('<span class="error">Email accepted.</span>');
                return false;
            }
});
});
</script>
</html>
